$(function() {

  $(document).ready(function() {

    var myAwesomeSlider = $('#vertical').lightSlider({
        addClass: "product-preview-slider",
        controls: false,
        loop: true,
        gallery:true,
        item:1,
        vertical:true,
        verticalHeight:440,
        vThumbWidth:60,
        thumbItem:5,
        thumbMargin:20,
        slideMargin:20,
        responsive : [
            {
                breakpoint:768,
                settings: {
                    item: 1,
                    // vertical:false
                  }
            }
        ]
      });


      $('.product-preview-next-slide').click(function(){
          myAwesomeSlider.goToNextSlide();
      });

  });

  // Truncate Default
  $('.product-description__content').truncate({
    lines: 2
  });

  // Truncate toggle
  $('.product-description__cut').on('click',function () {
    if ($(this).hasClass('uncutME')) {
      $('.product-description__content').truncate('collapse');
      $(this).removeClass("uncutME");
      $(this).text("полный текст");
    }
    else {
      $('.product-description__content').truncate('expand');
      $(this).addClass("uncutME");
      $(this).text("сократить текст");
    }

	});

  $('.menu-burger-wrapper').on('click',function () {
    $(this).toggleClass("cross-burger");
    $('.header-menu').toggleClass("show-menu");
	});

});
